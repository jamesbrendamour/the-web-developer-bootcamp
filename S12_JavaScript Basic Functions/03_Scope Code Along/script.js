// Example 

var phrase = "hi there!";

function doSomething() {
    var phrase = "Goodbye";
    console.log(phrase);
}

// Uses the var inside function
doSomething();
// doSomething does not change the parent var 

console.log(phrase);

console.log("\n\nScope Quiz\n\n");