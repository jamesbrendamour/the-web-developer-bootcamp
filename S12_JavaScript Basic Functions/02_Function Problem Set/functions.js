//Func for seeing if number is even or not 
console.log("isEven(num) Fuction");

function isEven(num) {
    if (num % 2 === 0) {
        return true;
    } else {
        return false;
    }
}

console.log(isEven(24));
console.log(isEven(37));

console.log("\nfactorial() function:");

function factorial(num) {

    var fact = 1;

    for (var i = num; i >= 0; i--) {
        if (i === 0) {
            console.log("Factorial of 0 = 1");
            fact;

        } else {
            fact = fact * i;

        }
    }
    return fact;

}

console.log(factorial(5));
console.log(factorial(10));

// kebabToSnake() Function
console.log("\nkebabToSnake() Function:");

function kebabToSnake(kebebStr) {
    var newStr = kebebStr.replace(/-/g, "_"); // /-/g find all the -
    return newStr;

}

console.log(kebabToSnake("james-douglas-brendamour"));