// Declaring the Function

function doSomething() {
    console.log("FIRST FUNCTIONS");
}

doSomething();
doSomething();
doSomething();
//To see the code don’t add the semicolon in console

// Say Hello fun that takes name

function sayHello(name) {
    console.log("Hello there " + name + "!");
}
sayHello("James");

//Multiple Args
function great(person1, person2, person3) {
    console.log("Hi " + person1);
    console.log("Hi " + person2);
    console.log("Hi " + person3);
}

great("James", "Alexa", "Mom")

// Squared function with return 
function square(num) {
    console.log("Squaring " + num);
    console.log(num * num);
    return num * num;
}

var numSquare = square(5);
console.log(numSquare);

// Func that Capitalizes the first char in a string

function capitalize(str) {
    console.log(str);
    return str.charAt(0).toUpperCase() + str.slice(1);
}

console.log(capitalize("james"));

//Function Exercise

function test(x, y) {
    return y - x;
}


console.log(test(10, 40));