var express = require("express");

var app = express();
//"/" -> "Hi There!"
// "/" root path
// req and res are objects 
app.get("/", function (req, res) {
    console.log("init Request");
    res.send("Hey There!");
});


app.get("/bye", function (req, res) {
    console.log("Bye Request");
    res.send("Goodbye!");

});

app.get("/dog", function (req, res) {
    console.log("Dog Request");
    res.send("MEOW!");
});


//Tell Express to listen for requests (starts server)
app.listen(3000, function () {
    console.log("Server Has Started");
});




