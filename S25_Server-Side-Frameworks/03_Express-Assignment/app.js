//Imports
var express = require('express');
var app = express();

// * This has imortant methods for doing requests
// '/' -> Hi There, Welcome to my assignment
app.get("/", function (req, res) {
    console.log("Hi There, Welcome to my assignment");
    res.send("Hi There, Welcome to my assignment");

});

//Visiting "/speak/pig" -> "The pig says Oink"
//"/speak/cow" -> "The Cow Says 'Moo'"
// "/speak/dog" -> "The dogs say wolf"

app.get("/speak/:animal", function (req, res) {

    var sounds = {
        pig: "Oink",
        cow: "moo",
        dog: "Woof Woof",
        cat: "I Hate you Human",
        goldish: "..."

    }

    var animal = req.params.animal.toLowerCase();
    var sound = sounds[animal];
    res.send("The " + animal + " says " + sound);
    console.log("The " + animal + " says " + sound);



});


// "/repeat/hello/3" -> "hello hello hello"
// "/repeat/hello/5" -> 5*hello
// "/repeat/blah/2" -> "blah blah"
app.get("/repeat/:message/:times", function (req, res) {
    var message = req.params.message;
    var times = Number(req.params.times);
    var result = "";

    for (var i = 0; i < times; i++) {
        result += message + " ";
    }
    res.send(result);
});

// Wrong URL -> "Sorry, page not found... what are you doing with your life"
app.get("*", function (req, res) {
    res.send("Sorry, page not found... what are you doing with your life");
    // res.send(req.param);
    console.log("Sorry, page not found... what are you doing with your life");
});

//Starting express server 
app.listen(3000, function () {
    console.log("Server Has Started");
});