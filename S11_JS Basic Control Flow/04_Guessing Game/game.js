//Secret number
var secretNumber = 4;

// ask for user to guess the number 

var guess = prompt("Guess a number");

// Prompt returns type string need to convert to number 
console.log(typeof (guess));
//Converts String --> Number
var guessNumber = Number(guess);

//Check if guess is right 
if (guessNumber === secretNumber) {
    alert("You Got it Right!!!");
} else if (guessNumber > secretNumber) {
    alert("Guess is greater than the number")
} else {
    alert("Guess is less then the number")
}