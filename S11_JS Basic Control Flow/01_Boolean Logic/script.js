//Exercise 1
var x = 10;
var y = "a"

var answer1 = y === "b" || x >= 10 //True
console.log(answer1);
// Exercise 2

var x = 3;
var y = 8;

var answer2 = !(x == "3" || x === y) && !(y != 8 && x <= y) //False

console.log(answer2);

// Exercise 3
var str = "";
var msg = "haha!";
var isFunny = "false";

var answer = !((str || msg) && isFunny);

console.log(answer);