// For Loops

var str = "hello";

// Iterate through string 
for (var i = 0; i < str.length; i++) {
    console.log(str[i]);

}

// Exercise 1
console.log("For Loop Exercises ");

// Loop through ever other number
for (let i = 0; i < 16; i += 2) {
    console.log(i);
}

console.log("\nFor Loops Problem Set:\n");

// Print all numbers between - 10 and 19
console.log("\nPrint all numbers between - 10 and 19");

for (var i = -10; i < 20; i++) {
    console.log(i);
}

// Print all even# between 10 and 40
console.log("\nPrint all even# between 10 and 40");

for (var i = 10; i <= 40; i++) {
    if (i % 2 === 0) {
        console.log(i);
    }
}

// Print all odd# between 300 and 333
console.log("\nPrint all odd# between 300 and 333");
for (var i = 300; i <= 333; i++) {
    if (i % 2 !== 0) {
        console.log(i);
    }
}
// Print all# divisible by 5 and 3 between 5 and 50
console.log("\nPrint all# divisible by 5 and 3 between 5 and 50");

for (var i = 5; i <= 50; i++) {
    if ((i % 5 === 0) && (i % 3 === 0)) {
        console.log(i);
    }
}