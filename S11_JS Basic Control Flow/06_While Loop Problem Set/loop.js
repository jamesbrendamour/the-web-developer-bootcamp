// //Secret number
// var secretNumber = 4;

// // ask for user to guess the number 

// var guess = prompt("Guess a number");

// // Prompt returns type string need to convert to number 
// console.log(typeof (guess));
// //Converts String --> Number
// var guessNumber = Number(guess);

// //Check if guess is right 
// if (guessNumber === secretNumber) {
//     alert("You Got it Right!!!");
// } else if (guessNumber > secretNumber) {
//     alert("Guess is greater than the number")
// } else {
//While loops 
// Print all numbers between - 10 and 19
var i = -10;
console.log("Printing all $ Between (-10,19)");
while (i < 20) {
    console.log(i);
    i++;
}
// Print all even# between 10 and 40

i = 10;
console.log("Printing all even # between (10,40)");
while (i <= 40) {
    //Checking if even
    if (i % 2 === 0) {
        console.log(i);
    }
    i++;
}

// Print all odd# between 300 and 333
i = 300;
console.log("Printing all odd # between (300,333)");
while (i <= 333) {

    // checking if odd
    if (i % 2 !== 0) {
        console.log(i);
    }
    i++;
}

// Print all# divisible by 5 and 3 between 5 and 50

i = 5;
console.log("Printing all # divisible by 5 and 3 between (5,50) ");
while (i <= 50) {

    if ((i % 5 === 0) && (i % 3 === 0)) {
        console.log(i);
    }
    i++;
}