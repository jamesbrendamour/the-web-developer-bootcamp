// //Secret number
// var secretNumber = 4;

// // ask for user to guess the number 

// var guess = prompt("Guess a number");

// // Prompt returns type string need to convert to number 
// console.log(typeof (guess));
// //Converts String --> Number
// var guessNumber = Number(guess);

// //Check if guess is right 
// if (guessNumber === secretNumber) {
//     alert("You Got it Right!!!");
// } else if (guessNumber > secretNumber) {
//     alert("Guess is greater than the number")
// } else {
//     alert("Guess is less then the number")
//}

//While Loop 
var count = 1;

while (count < 6) {
    console.log("Count is: " + count);
    count += 1;
}

//Loop Through String 
var str = "hello";
count = 0;

while (count < str.length) {
    console.log(str[count]);
    count++;
}

var num = 1;

while (num <= 20) {
    if (num % 4 === 0) {
        console.log(num);
    }
    num++;
}