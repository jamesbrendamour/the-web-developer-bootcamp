// Basic Array

// define Empty array
var friends = [];

// Also can define array useing 
var random = new Array();

// Adding First elements 
friends = ["Charlie", "Liz", "David", "Mattias"];



// Printing Full Array
console.log(friends);

// Printing single Element 
// Starting index is 0

console.log(friends[0]);


console.log(friends[1] + " <3 " + friends[2]); //"Liz <3 David"

// Update array elements by index
console.log("\nUpdating Array by Index");
friends[0] = "Chucky";
friends[1] = "Lizzie";

console.log(friends);

//Add another element at end of array
console.log("\nAppend New Element to Array");
friends[4] = "Amelie";
console.log(friends);

console.log("\nMultiple Data types in array");
random = [49, true, "James", null];
console.log(random);
console.log("Array Length " + random.length + " But index goes from 0 to " + (random.length - 1));

console.log("Getting the length of element at friends[0].length = " + friends[0].length);