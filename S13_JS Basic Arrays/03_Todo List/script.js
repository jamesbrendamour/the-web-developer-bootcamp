//list todo
var todos = ["Buy New Turtle"];

var input = prompt("What would you like to do 'list', 'new', 'quit'")



while (input !== "quit") {
    //handle input
    if (input === "list") { // outputs new todo

        todos.forEach(listTodo);

    } else if (input === "new") { // Adds new todo
        addTodo();
    } else if (input === "delete") {
        deleteTodo();

    }

    // ask again for new input
    input = prompt("What would you like to do 'list', 'new', 'quit'")
}
console.log("OK you Quit the app");

//Function for listing todo list
function listTodo(todo, i) {
    console.log(i + ". " + todo);
}

function addTodo() {
    // ask for new todo
    var newTodo = prompt("Enter New todo");
    console.log("Adding " + newTodo + " to Todo list");
    // add to todo array
    todos.push(newTodo);
}

function deleteTodo() {
    //Ask for index of todo to be delete
    var index = prompt("Enter index of todo to delete: ");
    console.log("Deleting Todo Time: " + todos[index]);
    // delete that todo
    todos.splice(index, 1);
}