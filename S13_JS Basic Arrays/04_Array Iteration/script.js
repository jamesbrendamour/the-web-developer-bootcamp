
// Array Iteration
var colors = ["red", "orange", "yellow", "green"]
// for loop iteration
console.log("Array Iteration:");
console.log("\nFor Loop Iteration");

for (var i = 0; i < colors.length; i++) {
    console.log(i + ". " + colors[i]);
}

console.log("----------------------------------------");
// forEach

console.log("\nforEach:");
colors.forEach(function (colors) {
    console.log(colors);
});

// Not just printing 

colors.forEach(function (iLoveDogs) {
    console.log("INSIDE THE FOREACH " + iLoveDogs);

});

// Using it with predefined function 
function printColor(color) {
    console.log("*************");
    console.log(color);
    console.log("*************");
}

// Calls the print colors function for use
colors.forEach(printColor);