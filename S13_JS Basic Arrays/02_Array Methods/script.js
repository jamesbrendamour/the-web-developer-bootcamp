//Array Methods
//Push and Pop

var colors = ["red", "orange", "yellow"]

console.log(colors);

// Built in method for adding something to the end of an array 

console.log("\nPush and Pop:");

console.log("\nPush: append item to array");

// Push 
var pushReturn = colors.push("Green");
console.log(colors);
console.log(pushReturn);

// Pop
console.log("\nPop: Removes and returns last item of array");
var popReturn = colors.pop();
console.log(colors);
console.log(popReturn);

// Unshift - Add to front 
console.log("\nUnshift: - add to front");
var unshiftReturn = colors.unshift("black");
console.log(colors);
console.log(unshiftReturn);

// Shift - remove front item
console.log("\nShift: - remove front item");
var shiftReturn = colors.shift();
console.log(colors);
console.log(shiftReturn);

// IndexOf - finds the index of an item in an array
console.log("\nIndexOf - finds the index of an item in an array");
console.log(colors);
console.log("Index of yellow: " + colors.indexOf("yellow"));
console.log("Returns when not in array: " + colors.indexOf("james"));

// Slice: To copy parts of an array
colors.push("black");
colors.push("purple");
console.log("\nSlice: To copy parts of an array");
console.log(colors);
var newArray = colors.slice(1, 4);
console.log(newArray);


