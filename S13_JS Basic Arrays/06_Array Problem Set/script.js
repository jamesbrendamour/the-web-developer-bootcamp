
// Array Iteration Challenge

var numbers = [2, 4, 5, 7, 4, 3, 9]
var same = [3, 3, 3, 3, 3]
var colors = ["red", "orange", "yellow", "green"]


// printReverse()
// Take an array as arg and prints our the elements in reverse order
function printReverse(array) {
    var length = array.length

    for (var i = (length - 1); i >= 0; i--) {
        console.log(array[i]);
    }
}

printReverse(numbers)


// isUniform()
// takes in array and tells if all ellements are equal

function isUniform(array) {
    for (var i = 1; i < array.length; i++) {
        if (array[i] !== array[i - 1]) {
            console.log("Array is not equal");
            return false;
        }
    }
    console.log("Array is Uniform");
    return true;
}


isUniform(numbers);
isUniform(same);


// sumArray()
// arg - array of numbers
// Returns the sum



// max()
// Return max number in array

function max(array) {
    var max = array[0];
    for (var i = 0; i < array.length; i++) {
        if (array[i] >= max) {
            max = array[i];
        }
    }
    console.log("Max Number is " + max);
}

max(numbers);

