//Clicks

var h1 = $("h1");

//Adding event listener for for h1 click
h1.click(function () {
    console.log("H1 Clicked");
});

//Listener to buttons
$("button").click(function () {
    console.log("Button Clicked!");
    $(this).css("background", "pink");
    var text = $(this).text();
    console.log("You Clicked " + text);
});

//Adding click listeners
h1.on("click", function () {
    $(this).css("color", "purple");
});

// for keypress
$("input").keypress(function (e) {
    console.log("keypress");
});

//hover over button

$("button").mouseenter(function () {
    console.log("Mouse Enter");
    $(this).css("font-weight", "bold");
});

$("button").mouseleave(function () {
    console.log("Mouse Enter");
    $(this).css("font-weight", "normal");
});