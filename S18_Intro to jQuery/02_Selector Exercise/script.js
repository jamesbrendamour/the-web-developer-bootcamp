//setting all divs to have background purple
$("div").css("background", "purple");

//All divs with class highlight = 200px width
$("div.highlight").css("width", "200px");

// id = third have border
$("#third").css("border", "2px orange solid");

//select first div and make font color pink
$("div:first").css("color", "pink");
