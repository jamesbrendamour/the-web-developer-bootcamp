

//Getting pic width
var img = $('img');
var input = $('input');

console.log($('img').css("width"));

//Changing width to 300px
img.css("width", "300px");

//Only change first image 
// $("img:first-of-type").css("width", "500px");
img.first().css("width", "500px");
//changing the last
img.last().css("width", "100px");

// seeing input attributes
console.log(input.attr('type'));
//changing input type
input.attr('type', "checkbox");
input.attr('type', "text");

//val() get the value of an element
// gets or changes value of input
console.log($('input').val());
//Reset input
$('input').val("");
$('input').val("Rusty Steele");

//Option selector
console.log($("select").val());
