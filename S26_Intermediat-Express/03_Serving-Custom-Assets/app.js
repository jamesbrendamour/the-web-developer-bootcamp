var express = require("express");
var app = express();

// Adding the public dir to the express server

app.use(express.static("public"));

//telling the file type of webpage for express so dont need to add it to ex. love.ejs
app.set("view engine", "ejs");

app.get("/", function (req, res) {
    res.render("home.ejs");
});

app.get("/love/:thing", function (req, res) {
    var thing = req.params.thing;
    // thingVar is = thinge
    res.render("love.ejs", { thingVar: thing });
});

app.get("/posts", function (req, res) {
    // Array of posts 
    var posts = [
        { title: "Post 1", author: "Susy" },
        { title: "My adorable pet bunny", author: "Charlie" },
        { title: "Can you believe this pomsky?", author: "Colt" }
    ];

    res.render("posts", { posts: posts });
})

app.listen(3000, function () {
    console.log("Server Has Started");
});