# VSCode Keyboard shortcuts

1. Search for Files In the Project: (CTRL+P)
2. Search and Rename:
   * Find all cases of func for var: (Shit + F12)
   * Rename in Current File: (Ctrl + H)
   * Rename In All of The Project: (Ctrl + Shift + H)
3. Navigation Inside Files:
   * Nav Outline: F1 --> @:
   * Nav to Line: F1 --> :###
   * End of File: (Ctrl + End)
   * Start of File: (Ctrl + Home)
4. Multiple Cursors: (Alt + Click)
5. Toggle Multi-line Comments: (Shift + Alt + A)
6. File Management:
   * Copy Relative Path: (Ctrl + K)
   * Copy Full Path: (Shift + Alt + C)

https://phpenthusiast.com/blog/10-must-know-vscode-shortcuts-and-tricks