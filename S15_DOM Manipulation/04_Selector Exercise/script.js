

//getElementById
// Takes a string argument and returns the one element with a matching ID
var tag = document.getElementById("highlight");
console.log(tag);

var tags = document.getElementsByClassName("bolded");

var body = document.getElementsByTagName("body")[0];
console.log(body);