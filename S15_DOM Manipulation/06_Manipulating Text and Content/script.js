
//Manipulating Text and Content

// textContent for paragraph
//select
var tag = document.querySelector("p");
tag = document.getElementsByTagName("p")[0];

//Retrieve the textContent
console.log(tag.textContent);

//Alter the textContent
tag.textContent = "James Brendamour"

// Select second ul

var ul2 = document.getElementsByTagName("ul")[1];
console.log(ul2.textContent);

//innerHTML
var p2 = document.getElementsByTagName("p")[1];
console.log(p2.innerHTML);
