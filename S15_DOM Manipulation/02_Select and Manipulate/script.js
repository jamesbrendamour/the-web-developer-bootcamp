
// Function inside an object 

var h1 = document.querySelector("h1");

h1.style.color = "pink";

//Select body 
var body = document.querySelector("body");
var isBlue = false;

// Changing to blue if it is not blue
setInterval(function () {
    if (isBlue) {
        body.style.background = "white";
    } else {
        body.style.background = "orange";
    }
    isBlue = !isBlue;
}, 1000);
