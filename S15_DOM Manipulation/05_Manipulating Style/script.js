
//Select 
var tag = document.getElementById("highlight");

//Manipulate
tag.style.color = "blue";
tag.style.border = "10px solid red";

// Add a class to the selected element 
tag.classList.add("another-class");
// Remove a class
tag.classList.remove("another-class");
//Toggle a class
tag.classList.toggle("another-class");

// Selecting paragraph 
var p = document.querySelector("p");

// Adding the big class to paragraph
p.classList.add("big");


