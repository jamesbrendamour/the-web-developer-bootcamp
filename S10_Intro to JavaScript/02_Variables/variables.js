var name = "Rusty";
var secretNumber = 73;
var inAdorable = true;

var hello = "Hello there ";

var helloName = hello + name;

console.log(helloName);

var num = 43;

console.log(num % 2);

// Changing var types 
console.log("num is a number:" + num);
num = "String";

console.log("num is now a: " + num);