// Numbers
console.log(4);
console.log(9.3)

// Strings
console.log("Strings: Single or Double Quotes OK");
console.log('43');
console.log("Hello World");

// Concatenation
console.log('Concatenation');
console.log('charlie' + 'brown');

//Escape Characters start with "\"
console.log("Escape Characters start with  \\");

console.log("James said \"Hello\"");

// Strings have a length property 
console.log("Strings have a length property");
console.log("\"Hello World\" Length:");
console.log("Hello World".length);

//Access individual characters using [] and an index 
console.log("Length starts at 1 but position start at 0: so the last char of a string is length -1");
console.log("Access individual characters using [] and an index");
console.log("Fist char of hello = \"Hello\"[0]");
console.log("Hello" [0]);

//Booleans
console.log("Booleans:");
console.log(true);
console.log(false);


//null and undefined - are actually values
console.log("null and undefined - are actually values");
console.log(null);
console.log(undefined);

console.log("Can do Some Math");
console.log(4 + 10);
console.log(1 / 5);

console.log("Modulo - remainder operator");
console.log("10 % 3:");
console.log(10 % 3);
console.log("24 % 2:");
console.log(24 % 2);