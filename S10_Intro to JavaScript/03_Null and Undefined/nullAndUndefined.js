// Vars that are declared but not initialized are undefined
// For Example :
console.log("Undefined Var Example");

var name;
console.log(name);

// null is "explixitly nothing"
console.log("Null is explixity nothing");

name = null;

console.log(name);