

// Movie DataBase

var movie = [
    {
        title: "Cars",
        rating: "4",
        watched: true
    },
    {
        title: "Frozen",
        rating: "4.5",
        watched: false
    }
]

console.log(movie);

movie.forEach(function (movie) {
    buildString(movie)
});

//Prints movie info
function buildString(movie) {
    var result = "You have ";

    if (movie.watched) {
        result += "watched";
    } else {
        result += "not seen";
    }
    result += " " + movie.title + " - ";
    result += movie.rating + " stars";
    console.log(result);
}
