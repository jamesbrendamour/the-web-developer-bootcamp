
// Function inside an object 

var obj = {
    add: function (x, y) {
        return x + y;
    }
}

console.log(obj.add(4, 3));

//This Example 

var comments = {};

comments.data = ["hi", "james", "mom"]; // list of data

console.log(comments);

// print(comments.data)
function print() {
    this.data.forEach(function (el) {
        console.log(el);
    });
}