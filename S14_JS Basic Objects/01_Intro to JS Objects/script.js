
// Object
var person = {
    name: "Travis",
    age: 21,
    city: "LA"
};

var dog = {
    name: "Rusty",
    breed: "mutt",
    age: 3
}

console.log(person["name"]);
console.log(person.age);

person.name = "James";

// Creating Object 

var obj = {};

obj.type = "Car";
obj.brand = "audi";
console.log(obj);

//Changing Object 
console.log(person);

var posts = [
    {
        title: "Cants are ok",
        author: "James",
        comment: ["Awesome Post", "terrible Post"] // array of comments inside the object
    },
    {
        title: "Cats are Awesome",
        author: "Jim",
        comment: ["<3", "Go to hell"] // array of comments inside the object


    }
]

console.log(posts);

console.log(posts[0].title);
console.log(posts[1].comment[1]);