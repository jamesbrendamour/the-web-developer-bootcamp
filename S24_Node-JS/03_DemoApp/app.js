console.log("From App.js");

//import "cat-me";

var cat = require("cat-me");
var jokes = require("knock-knock-jokes");
var fake = require("faker");

console.log(cat);

console.log(cat());
console.log(cat('playful'));


console.log("Jokes");
console.log(jokes());

console.log("Faker Package:");
// console.log(fake);
console.log(fake['hacker']);

var name = fake.name.findName();
var email = fake.internet.email();
var card = fake.helpers.createCard();



console.log(name);
console.log(email);
console.log(card);

console.log("Product data:");

var productName = fake.commerce.productName();
var productPrice = fake.commerce.price();

console.log(productName);
console.log("$" + productPrice);
