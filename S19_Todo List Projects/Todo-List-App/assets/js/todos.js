
//Check Off Specific Todos By Click
$("ul").on("click", "li", function () {
    // console.log($(this).css("color"));
    console.log("Ckicked Todo List Item");
    $(this).toggleClass("completed");

});

//Adding the delete Item from list
$("ul").on("click", "span", function (e) {
    console.log("Clicked on Delete Button");
    $(this).parent().fadeOut(500, function () {
        $(this).remove();
    });
    //Stops the click of li from running
    e.stopPropagation();
});

//Listener for pressing enter after creating new todo list item
$("input[type='text']").keypress(function (e) {
    //checking if the enter key is hit
    if (e.which === 13) {
        console.log("Enter Key Pressed");
        var todoText = $(this).val();
        //checking that item isn't empty
        if (todoText === "") {
            alert("Todo Item must have name");

        } else {
            console.log("Adding " + todoText + " to ToDo List");
            $(this).val("")
            $("ul").append("<li><span><i class='fa fa-trash'></i></span></span> " + todoText + "</li>");
        }
    }
});

//Plus sign button 
//Hide and show text input
$(".fa-plus").click(function (e) {
    console.log("Clicked Plus Sign");
    $("input[type='text']").fadeToggle();
});


