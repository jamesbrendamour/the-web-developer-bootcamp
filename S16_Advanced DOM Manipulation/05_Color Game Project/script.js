//Color Game
var numSquares = 6;
var colors = generateRandomColors(numSquares);
//select all the class square
var squares = document.querySelectorAll(".square");
//Random index picked
var pickedColor = pickColor();
var colorDisplay = document.getElementById("colorDisplay");
// selecting Message display for when guess correct or wrong
var messageDisplay = document.getElementById('message');
var h1 = document.querySelector('h1');
var resetButton = document.getElementById('reset');
// var easyBtn = document.getElementById('easyBtn');
// var hardBtn = document.getElementById('hardBtn');
var modeButtons = document.querySelectorAll(".mode");
//Listener for reset button
resetButton.addEventListener("click", resetColors);

//Mode button listeners
for (var i = 0; i < modeButtons.length; i++) {
    modeButtons[i].addEventListener("click", function () {
        console.log("Mode Button Clicked");
        modeButtons[0].classList.remove("selected");
        modeButtons[1].classList.remove("selected");
        this.classList.add("selected");

        //Changing the numSquare for the mode. Like an if statement
        this.textContent === "Easy" ? numSquares = 3 : numSquares = 6;

        resetColors();

    });
}
// Adding Color to title 
colorDisplay.textContent = pickedColor;

for (var i = 0; i < squares.length; i++) {
    //add initial colors to squares
    squares[i].style.backgroundColor = colors[i];

    // add click listeners to squares
    squares[i].addEventListener("click", colorClick);

}

//Reset button function
function resetColors() {
    //generate all new colors
    colors = generateRandomColors(numSquares);
    //pick a new random color from array
    pickedColor = pickColor();
    //change color display to match picked color
    colorDisplay.textContent = pickedColor;
    for (var i = 0; i < squares.length; i++) {
        if (colors[i]) {
            squares[i].style.display = "block";
            squares[i].style.backgroundColor = colors[i];

        } else {
            squares[i].style.display = "none";
        }
    }
    h1.style.backgroundColor = "#232323";
    messageDisplay.textContent = "";
    resetButton.textContent = "New Colors";


}

//Clicked Events Color
// Right change background to the color
function colorClick() {
    var clickedColor = this.style.backgroundColor;
    console.log(clickedColor, pickedColor);
    if (clickedColor === pickedColor) {
        console.log("Correct!");
        messageDisplay.textContent = "Correct!";
        // Changing all the squares to the correct color background
        changeColors(clickedColor);
        //Changing h1 background 
        h1.style.backgroundColor = pickedColor;
        resetButton.textContent = "Play Again";
    } else {
        console.log("Wrong");
        //Change color square to background color when incorrect
        this.style.backgroundColor = "#232323";
        messageDisplay.textContent = "Try Again"
    }

}

// changes the colors 
function changeColors(color) {
    //loop though all squares 
    for (var i = 0; i < squares.length; i++) {
        //change each color to match given color
        squares[i].style.backgroundColor = color;

    }

}

//random color function
function pickColor() {
    //Random number between 1 - 6 
    var random = Math.floor((Math.random() * colors.length) + 1);
    return colors[random];
}

//Generates all the random colors random colors 
function generateRandomColors(num) {
    //make an array
    var arr = [];
    // add num random colors to array
    for (var i = 0; i < num; i++) {
        //gets random color and push it into the array
        arr.push(randomColor());

    }
    return arr;
}

//generates a single random color 
function randomColor() {
    // pick red, green and blue from 0 -255
    var r = Math.floor(Math.random() * 256);
    var g = Math.floor(Math.random() * 256);
    var b = Math.floor(Math.random() * 256);

    // "rgb(r, g, b)""
    return "rgb(" + r + ", " + g + ", " + b + ")";

}