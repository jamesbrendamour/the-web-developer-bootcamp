
//Select button and Paragraph

var p = document.querySelector("p");

//Listens to the button for when it gets clicked 

// Change h1 when clicked on 
var h1 = document.querySelector("h1");

h1.addEventListener("click", function () {
    alert("h1 was clicked");
});

//Listener for ul
var ul = document.querySelector("ul");

ul.addEventListener("click", function () {
    console.log("You Clicked the UL");
});

//Listener for li inside ul 
var lis = document.querySelectorAll("li");
console.log(lis);

for (var i = 0; i < lis.length; i++) {
    lis[i].addEventListener("click", function () {
        console.log(this.textContent);
    });
}

// Can yous a declared function too
var button = document.querySelector("button");

button.addEventListener("click", changeText);
function changeText() {
    p.textContent = "Someone Clicked the Button"
}



