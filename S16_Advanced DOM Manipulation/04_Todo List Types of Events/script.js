//
//Select First li

var lis = document.querySelectorAll("li");

//Add Event Listener for hover and hover out
// loop though all the li's
for (var i = 0; i < lis.length; i++) {
    lis[i].addEventListener("mouseover", mouseOver);

    lis[i].addEventListener("mouseout", mouseOut);
    // adding click listener
    lis[i].addEventListener("click", done);

}


//Mouse Over 
function mouseOver() {
    this.classList.add("selected");
}

//Mouse Out
function mouseOut() {
    this.classList.remove("selected");
}

//click function
function done() {
    this.classList.toggle("done");
}
