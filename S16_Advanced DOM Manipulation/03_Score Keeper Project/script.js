//Score Keeper

//Stops after someone reaches 5
// can decide what to play to 
//Selecting the different elements
var p1 = document.querySelector('#p1');
var p2 = document.getElementById('p2');
var reset = document.getElementById('reset');

//Selecting the score display
var p1Display = document.getElementById('p1Display');
var p2Display = document.getElementById('p2Display');
var numInput = document.querySelector('input');
var playToDisplay = document.getElementById('playTo');
//Setting Starting Score
var p1Score = 0;
var p2Score = 0;
//var to keep track of game state
var gameover = false;
var winningScore = 5;

//Adding Listeners 
//Clicking button for player 1
p1.addEventListener("click", p1Clicked);

//Listener for Player 2
p2.addEventListener("click", p2Clicked);

//Listener for reset 
reset.addEventListener("click", resetClicked);

//Play to Listener 
numInput.addEventListener("change", playTo);


//Functions for button clicks
function playTo() {
    winningScore = Number(numInput.value);
    console.log("Changing Winning Score to " + winningScore);

    playToDisplay.textContent = winningScore;
    resetClicked();

}

function p1Clicked() {
    if (!gameover) {
        console.log("Point for Player 1");
        p1Score++;
        if (p1Score === winningScore) {
            console.log("GAME OVER: Player 1 wins");
            //adding class winner to text
            p1Display.classList.add("winner");
            p2Display.classList.add("losser");
            //Setting Game Over Var
            gameover = true;

        }
        p1Display.textContent = p1Score;

    }


}

function p2Clicked() {
    if (!gameover) {
        console.log("Point for Player 2");
        p2Score++;
        if (p2Score === winningScore) {
            console.log("GAME OVER: Player 2 Wins ");
            p2Display.classList.add("winner");
            p1Display.classList.add("losser");
            gameover = true;
        }
        p2Display.textContent = p2Score;
    }

}

function resetClicked() {
    console.log("Resetting Score");

    //Resetting vars
    gameover = false;
    p1Score = 0;
    p2Score = 0;
    //Reset Score on html display
    p1Display.textContent = p1Score;
    p2Display.textContent = p2Score;

    //Reset the css class winner
    p1Display.classList.remove('winner');
    p2Display.classList.remove('winner');
    //remove the loser class
    p1Display.classList.remove('losser');
    p2Display.classList.remove('losser');



}