
// Selecting button

var bt = document.getElementsByTagName("button")[0];

bt.addEventListener("click", toggleColor);


//Toggle Color Function
var ispurple = false;
function toggleColor() {
    // if white turn purple else turn purple
    if (ispurple) {
        document.body.style.background = "white";
    } else {
        document.body.style.background = "purple";
    }
    ispurple = !ispurple;

}

//Toggle with Method 
var bt2 = document.getElementsByTagName("button")[1];
bt2.addEventListener("click", toggleBlack);

function toggleBlack() {
    //toggles the class name black in app.css
    document.body.classList.toggle("black");
}

